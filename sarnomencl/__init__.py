#!/usr/bin/env python

from builtins import str
from past.builtins import basestring
from builtins import object
import os
import glob
import datetime
import re
from string import Template
import itertools
import sys
import logging
import pdb

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


# base_path="/home/cercache/project/mpc-sentinel1/data/esa"


class Nomencl(object):
    def __init__(self, regex, template, defaultsValues=None):
        """
        regex   :   regex of the nomenclature, with grouping
        template:   template of the nomenclature. must use ${var} notation. ${var} and regex groups must match.
        """
        self._logger = logging.getLogger(self.__class__.__name__)
        self._regex = regex
        self._template = template
        self._defaultsValues = defaultsValues
        self._tags = re.findall(r"\$\{([\w]+)\}", self._template.template)
        self._parsed = False

        self._dict = {}
        for tag in self._tags:
            self._dict[tag] = "*"
        # self._string    =   str(self)

    def setTags(self, **kwargs):
        if self._defaultsValues:
            # some tags implies other tags
            for superTag in set.intersection(
                set(kwargs.keys()), set(self._defaultsValues.keys())
            ):
                userSuperTagValue = kwargs[superTag]
                if userSuperTagValue in self._defaultsValues[superTag]:
                    for defaultKey in self._defaultsValues[superTag][userSuperTagValue]:
                        if defaultKey not in kwargs:
                            # set default only if not specified by user
                            self._logger.debug(
                                "SuperTag %s %s: implie setting %s to %s"
                                % (
                                    superTag,
                                    userSuperTagValue,
                                    defaultKey,
                                    self._defaultsValues[superTag][userSuperTagValue][
                                        defaultKey
                                    ],
                                )
                            )
                            kwargs[defaultKey] = self._defaultsValues[superTag][
                                userSuperTagValue
                            ][defaultKey]
        for key in kwargs:
            if key in self._dict:
                self._dict[key] = kwargs[key]
            else:
                self._logger.warning("Ignoring unknown key %s" % key)
        return self

    def getTag(self, tag):
        if tag not in self._dict:
            raise Exception(
                "Unable to get tag %s. Known tags : %s" % (tag, list(self._dict.keys()))
            )
        return self._dict[tag]

    def getTags(self):
        return self._dict

    def isParsed(self):
        return self._parsed

    def setString(self, string):
        regex = self._regex.search(string)
        if regex:
            for tag in self._tags:
                self._dict[tag] = regex.group(self._tags.index(tag) + 1)
            self._parsed = True
        else:
            raise Exception(
                "Unable to find nomencl %s in %s" % (str(self._regex.pattern), string)
            )
        return self

    def __str__(self):
        if self._parsed:
            return self._template.substitute(self._dict)
        else:
            return ""


# S1A_IW_GRDH_1SDV_20201103_234136_20201103T234201T035091_04184A_051C.SAFE
# S1A_IW_GRDH_1SDV_20170731T100948_20170731T101013_017714_01DAA9_27E2.SAFE
class SafeNomencl(Nomencl):
    regex = re.compile(
        "(...)_(..)_(...)(.)_(.)(.)(..)_(........T......)_(........T......)_(......)_(......)_(....).SAFE"
    )
    template = Template(
        "${MISSIONID}_${BEAM}_${PRODUCT}${RESOLUTION}_${LEVEL}${CLASS}${POL}_${STARTDATE}_${STOPDATE}_${ORBIT}_${TAKEID}_${PRODID}.SAFE"
    )
    defaultsValues = {
        "PRODUCT": {
            "OCN": {"RESOLUTION": "_", "LEVEL": "2", "PRODID": "*"},
            "GRD": {"RESOLUTION": "H", "LEVEL": "1", "PRODID": "*"},
        }
    }

    def __init__(self, safe):
        super(SafeNomencl, self).__init__(
            SafeNomencl.regex,
            SafeNomencl.template,
            defaultsValues=SafeNomencl.defaultsValues,
        )
        super(SafeNomencl, self).setString(safe)
        # self.nomencl    =   Nomencl(regex=regex , tags=tags , template=template)


# RS2_OK97458_PK855010_DK786985_SCWA_20140717_225349_VV_VH_SGF
# RS2_OK99999_PK999999_DK999999_XXXX_20130314_055132_VV_SGF
# RS2_OK99999_PK999999_DK999999_XXXX_20101014_090242_HH_HV_SGF
# RS2_OK101963_PK882792_DK815838_SCWA_20180913_105943_VV_VH_SGF
class RS2Nomencl(Nomencl):
    regex = re.compile(
        "(RS2)_OK([0-9]+)_PK([0-9]+)_DK([0-9]+)_(....)_(........)_(......)_(.._?.?.?)_(S.F)"
    )
    template = Template(
        "${MISSIONID}_OK${DATA1}_PK${DATA2}_DK${DATA3}_${DATA4}_${DATE}_${TIME}_${POLARIZATION}_${LAST}"
    )

    def __init__(self, safe):
        super(RS2Nomencl, self).__init__(RS2Nomencl.regex, RS2Nomencl.template)
        super(RS2Nomencl, self).setString(safe)


# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SCLNC/GRD/2023/143/RCM2_OK2565948_PK2568255_1_SCLNC_20230523_082958_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SCLNC/GRD/2023/173/RCM2_OK2614527_PK2618845_1_SCLNC_20230622_221053_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SCLNC/GRD/2023/065/RCM2_OK2466905_PK2468189_1_SCLNC_20230306_055334_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SC50MA/GRD/2021/247/RCM2_OK2381541_PK2383489_1_SC50MA_20210904_102223_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SC50MA/GRD/2021/254/RCM2_OK1532980_PK1777130_1_SC50MA_20210911_094302_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SC50MA/GRD/2021/254/RCM2_OK2381549_PK2382736_1_SC50MA_20210911_203802_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SC100MA/GRD/2021/231/RCM2_OK2378048_PK2383881_1_SC100MA_20210819_025419_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SC100MA/GRD/2021/230/RCM2_OK2378048_PK2383884_1_SC100MA_20210818_143919_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SCLNA/GRD/2022/360/RCM2_OK2411022_PK2411797_1_SCLNA_20221226_123933_VV_VH_GRD
# /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm2/SCLNA/GRD/2022/360/RCM2_OK2394533_PK2395703_1_SCLNA_20221226_123933_VV_VH_GRD
#                                                                                        RCM2_OK2377865_PK2377884_1_SC50MHIPRFA_20210216_204529_HH_HV_GRD
# RCM1_OK2377166_PK2377284_1_SC30MB_20200611_114145_HH_HV_GRD
class RCMNomencl(Nomencl):
    regex = re.compile(
        "(RCM)([0-9])_OK([0-9]+)_PK([0-9]+)_([0-9]+)_(\w+)_(........)_(......)_(.._?.?.?)_(...)"
    )
    template = Template(
        "${MISSIONID}${MISSIONNUMBER}_OK${DATA1}_PK${DATA2}_${DATA3}_${BEAM}_${DATE}_${TIME}_${POLARIZATION}_${LAST}"
    )

    def __init__(self, safe):
        super(RCMNomencl, self).__init__(RCMNomencl.regex, RCMNomencl.template)
        super(RCMNomencl, self).setString(safe)


class MeasurementNomencl(Nomencl):

    regex = re.compile(
        "(...)-(..)-(...)-(..)-(........t......)-(........t......)-(......)-(......)-(...).(.*)"
    )
    template = Template(
        "${missionid}-${beam}-${product}-${pol}-${startdate}-${stopdate}-${orbit}-${takeid}-${num}.${ext}"
    )
    defaultsValues = {"product": {"ocn": {"ext": "nc"}, "grd": {"ext": "tiff"}}}

    def __init__(self, measurement):
        super(MeasurementNomencl, self).__init__(
            MeasurementNomencl.regex,
            MeasurementNomencl.template,
            defaultsValues=MeasurementNomencl.defaultsValues,
        )
        super(MeasurementNomencl, self).setString(measurement)


class SafePathNomencl(SafeNomencl):
    template = Template(
        "sentinel-${MissionId}/L${LEVEL}/${BEAM}/${MISSIONID}_${BEAM}_${PRODUCT}${RESOLUTION}_${LEVEL}${CLASS}/${Year}/${Doy}"
    )

    def __init__(self, safe):
        super(SafePathNomencl, self).__init__(safe)
        # self._tags  =   super(SafePathNomencl,self).getTags()
        self._dict["StartDate"] = None
        self._dict["MissionId"] = "*"
        self._dict["Year"] = "*"
        self._dict["Doy"] = "*"
        try:
            self._dict["StartDate"] = datetime.datetime.strptime(
                self._dict["STARTDATE"], "%Y%m%dT%H%M%S"
            )
            self._dict["MissionId"] = self._dict["MISSIONID"][1:3].lower()
            self._dict["Year"] = self._dict["StartDate"].strftime("%Y")
            self._dict["Doy"] = self._dict["StartDate"].strftime("%j")
        except:
            pass

    def __str__(self):
        safePathNomenclStr = super(SafePathNomencl, self).__str__()
        if safePathNomenclStr:
            return "%s/%s" % (
                SafePathNomencl.template.substitute(self._dict),
                super(SafePathNomencl, self).__str__(),
            )
        else:
            return ""


class RS2PathNomencl(RS2Nomencl):
    template = Template("${MISSIONID}/L${LEVEL}/${POLARIZATION}/${Year}/${Doy}")

    def __init__(self, safe):
        super(RS2PathNomencl, self).__init__(safe)
        self._dict["StartDate"] = None
        self._dict["Year"] = "*"
        self._dict["Doy"] = "*"
        try:
            self._dict["StartDate"] = datetime.datetime.strptime(
                "%s %s" % (self._dict["DATE"], self._dict["TIME"]), "%Y%m%d %H%M%S"
            )
            self._dict["Year"] = self._dict["StartDate"].strftime("%Y")
            self._dict["Doy"] = self._dict["StartDate"].strftime("%j")
            self._dict["LEVEL"] = "1"  ## Maybe TOFIX
        except:
            pass

    def __str__(self):
        safePathNomenclStr = super(RS2PathNomencl, self).__str__()
        if safePathNomenclStr:
            return "%s/%s" % (
                RS2PathNomencl.template.substitute(self._dict),
                super(RS2PathNomencl, self).__str__(),
            )
        else:
            return ""


class RCMPathNomencl(RCMNomencl):
    # /home/datawork-cersat-public/provider/asc-csa/satellite/l1/rcm/rcm1/SC30MB/GRD/2020/163
    # template = Template(
    #    "${MISSIONIDLOWER}/${MISSIONIDLOWER}${MISSIONNUMBER}/${DATA4}/${LAST}/${Year}/${Doy}"
    # )

    # New path
    # /home/datawork-cersat-public/provider/csa/satellite/l1/rcm/rcm-1/sc30mb/2020/163/RCM1_OK2377166_PK2377284_1_SC30MB_20200611_114145_HH_HV_GRD
    template = Template(
        "${MISSIONIDLOWER}/${MISSIONIDLOWER}-${MISSIONNUMBER}/${BEAMLOWER}/${Year}/${Doy}"
    )

    def __init__(self, safe):
        super(RCMPathNomencl, self).__init__(safe)
        self._dict["StartDate"] = None
        self._dict["Year"] = "*"
        self._dict["Doy"] = "*"
        try:
            self._dict["StartDate"] = datetime.datetime.strptime(
                "%s %s" % (self._dict["DATE"], self._dict["TIME"]), "%Y%m%d %H%M%S"
            )
            self._dict["Year"] = self._dict["StartDate"].strftime("%Y")
            self._dict["Doy"] = self._dict["StartDate"].strftime("%j")
            self._dict["LEVEL"] = "1"  ## Maybe TOFIX
            self._dict["MISSIONIDLOWER"] = self._dict["MISSIONID"].lower()
            self._dict["BEAMLOWER"] = self._dict["BEAM"].lower()
        except:
            pass

    def __str__(self):
        safePathNomenclStr = super(RCMPathNomencl, self).__str__()
        if safePathNomenclStr:
            return "%s/%s" % (
                RCMPathNomencl.template.substitute(self._dict),
                super(RCMPathNomencl, self).__str__(),
            )
        else:
            return ""


class FullPathNomencl(object):
    # changing one of those tags will change both safe and measurement
    linked = ["product", "missionid", "beam", "orbit", "takeid"]  # TODO: pol

    # tags always uppercase
    # upperCase   =   ["takeid"]

    def __init__(self, fullPath, prefix=None):
        self._safePath = SafePathNomencl(fullPath)
        self._measurement = MeasurementNomencl(fullPath)
        self._prefix = prefix
        if not self._prefix:
            try:
                self._prefix = re.sub("(.*)%s.*" % str(self._safePath), r"\1", fullPath)
            except:
                self._prefix = ""

    def setTags(self, **kwargs):
        measurementTags = {}
        safePathTags = {}
        for tag in kwargs:
            if tag.lower() in FullPathNomencl.linked:
                measurementTags[tag.lower()] = kwargs[tag].lower()
                safePathTags[tag.upper()] = kwargs[tag].upper()
            else:
                if tag.lower() == tag:
                    measurementTags[tag] = kwargs[tag]
                if tag.upper() == tag:
                    safePathTags[tag] = kwargs[tag]

        # for tag in measurementTags:
        #    if tag in FullPathNomencl.upperCase:
        #        self._logger("to uppercase : %s" % tag)
        #        measurementTags[tag]=measurementTags[tag].upper()
        #
        # for tag in safePathTags:
        #    if tag in FullPathNomencl.upperCase:
        #        safePathTags[tag]=safePathTags[tag].upper()

        if measurementTags:
            self._measurement.setTags(**measurementTags)
        if safePathTags:
            self._safePath.setTags(**safePathTags)
        return self

    def getTags(self):
        tags = self._safePath.getTags().copy()
        tags.update(self._measurement.getTags())
        return tags

    def getTag(self, tag):
        tags = self.getTags()
        return tags[tag]

    def __str__(self):
        strSafePath = str(self._safePath)
        strMeasurement = str(self._measurement)
        if strSafePath and strMeasurement:
            return "%s%s/measurement/%s" % (self._prefix, strSafePath, strMeasurement)
        if strSafePath:
            return "%s%s" % (self._prefix, strSafePath)
        if strMeasurement:
            return strMeasurement

    def getFiles(self):
        return glob.glob(str(self))


default_prefix = [
    os.getenv(
        "S1_BASEDIR",
        "/home/datawork-cersat-public/cache/project/mpc-sentinel1/data/esa",
    ),
    os.getenv(
        "SARWING_DATA_ALT", "/home/datawork-cersat-public/cache/project/sarwing/data"
    ),
    os.getenv(
        "SARWING_DATA_RCM", "/home/datawork-cersat-public/provider/csa/satellite/l1"
    ),
]


def sar2path(name, skip=False, prefix=default_prefix):
    import past

    if isinstance(prefix, past.types.basestring):
        prefix = [prefix]

    # If the path is a netcdf file, simply return the same path
    if name.endswith(".nc") and os.path.isfile(name):
        return name

    if "RS2" in name:
        post_path = str(RS2PathNomencl(name))
    elif "SAFE" in name:
        post_path = str(SafePathNomencl(name))
    elif "RCM" in name:
        post_path = str(RCMPathNomencl(name))
    else:
        logger.warning("sar2path : no sar image reconized for %s " % name)
        return None

    paths = [os.path.join(pref, post_path) for pref in prefix]
    if skip:
        found = False
        for path in paths:
            try:
                if "rcm" not in path and os.listdir(path):  # non empty dir
                    found = True
                    break
                elif "rcm" in path:  # FIXME dirty fix because RCM path are not uniform
                    # /home/datawork-cersat-public/provider/csa/satellite/l1/rcm/rcm-1/sc30mb/2020/163/RCM1_OK2377166_PK2377284_1_SC30MB_20200611_114145_HH_HV_GRD
                    # /home/datawork-cersat-public/provider/csa/satellite/l1/rcm/rcm-1/sclna/2024/027/RCM1_OK2918747_PK2921400_1_SCLNA_20240127_012813_VV_VH_GRD/RCM1_OK2918747_PK2921400_1_SCLNA_20240127_012813_VV_VH_GRD
                    # sometimes the last part gets repeated, sometimes no.
                    splt = path.split("/")
                    last = splt[-1]
                    # Re-using last part of path and add it a second time to the path. Then, test if the dir exists.
                    to_test = os.path.join(path, last)
                    # If the dir exists, we use it.
                    if os.path.exists(to_test):
                        found = True
                        path = to_test
                        break
                    # If it doesn't, checking if the "normal" path exists and use it if so.
                    elif os.path.exists(path):
                        found = True
                        break
            except:
                pass
        if found:
            return path
        else:
            return None
    else:
        if (
            "rcm" in paths[0]
        ):  # FIXME dirty fix because RCM path are not uniform and aren't built like S1/RS2
            # The goal is to have like rcm-1/L1 in output path when constructing path
            splt = paths[0].split("/")
            splt.insert(-6, splt[-5])
            splt[-6] = "L1"
            splt[-5] = splt[-4]
            splt[-4] = "{}_{}".format(splt[-7], splt[-4])
            # del splt[-4]
            paths[0] = "/".join(splt)

            # /home/datawork-cersat-public/provider/csa/satellite/l1/rcm/rcm-1/sc30mb/2020/163/RCM1_OK2377166_PK2377284_1_SC30MB_20200611_114145_HH_HV_GRD
            # /home/datawork-cersat-public/provider/csa/satellite/l1/rcm/rcm-1/sclna/2024/027/RCM1_OK2918747_PK2921400_1_SCLNA_20240127_012813_VV_VH_GRD/RCM1_OK2918747_PK2921400_1_SCLNA_20240127_012813_VV_VH_GRD
            # sometimes the last part gets repeated, sometimes no.
            splt = paths[0].split("/")
            last = splt[-1]
            to_test = os.path.join(paths[0], last)
            if os.path.exists(to_test):
                return os.path.join(paths[0], last)
            else:
                return paths[0]

        return paths[0]
