from setuptools import setup 

setup(name='sarnomencl',
      description='Sar Nomenclature',
      url='https://gitlab.ifremer.fr/sarwing/sarnomencl.git',
      use_scm_version=True,
      author = "Theo Cevaer",
      author_email = "Theo.Cevaer@ifremer.fr",
      license='GPL',
      packages=['sarnomencl'],
      zip_safe=False,
      scripts=['bin/sar2path.py'],
      install_requires=["future"]
)
