#!/usr/bin/env python

from __future__ import print_function
import logging
import argparse
import datetime
import sys
import socket
import os
import sarnomencl

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

#logger.info("Script executed at %s" % datetime.datetime.now())
#logger.info("Called with the following command : %s" % " ".join(sys.argv))
#logger.info("The host machine is : %s" % socket.gethostname())

if __name__ == "__main__":
    description = "Generate L1 path from L1 name (RS2 or sentinel)"

    parser = argparse.ArgumentParser(description = description)

    parser.add_argument("--debug", action="store_true", default=False, help="start the script in debug mode")
    parser.add_argument('--prefix', action="append", type=str, help="base path prefix")
    parser.add_argument('--skip', action="store_true", default=False,
                        help="skip if output path doesn't exist. (print basename on stderr). It adds path verification"
                             " and chosing the one that exists among given prefixes.")
    parser.add_argument('names', action="store", nargs='*', type=str, help="sar image basename")
       

    args = parser.parse_args()
    
    if args.debug:
        try:
            import debug
        except:
            pass
    
    lines = []
    if len(args.names) == 0:
        lines = sys.stdin.readlines()
    else:
        lines = args.names

   
    for name in lines:
        if name.startswith('/'):
            # allready a path
            print(name.rstrip())
        else:
            path=sarnomencl.sar2path(name,skip=args.skip,prefix=args.prefix)
            if not path and args.skip:
                sys.stderr.write("%s\n" % name.rstrip())
            else:
                print(path)

    

#Old script, peut-etre que ca peut t'etre utile
# parse args
#wargs={}
#or arg in sys.argv[1:]:
#   (key,value) = arg.split('=')
#   kwargs[key.lstrip('-')]=value
#
#
#ount       =   0
#kCount     =   0
#ndColor    =   '\033[0m'
#or fname in sys.stdin:
#   fname=fname.rstrip()
#   count       =   count+1
#   newFname    =   sentinelnomencl.FullPathNomencl(fname).setTags(**kwargs)
#   print "FILES %s" % newFname.getFiles()
#   if newFname.getFiles():
#       okCount =   okCount+1
#       color   =   '\033[92m' # green
#   else:
#       color   =   '\033[91m' # red    
#   print "%s %s%s%s" % ( fname , color, newFname , endColor )
#
#ys.stderr.write("%d/%d converted (%2.0f%%)\n" % (okCount , count, float(okCount)/count*100))
